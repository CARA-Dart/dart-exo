
import 'dart:async';
import 'dart:html';

import 'package:stagexl/stagexl.dart';

import 'WorldStage.dart';
import 'data/WorldMap.dart';
import 'interfaces/IDisplayResource.dart';

abstract class AbstractDartStage extends Stage{

  static ResourceManager _resourceManager = new ResourceManager();
  WorldMap worldMap;

  AbstractDartStage(CanvasElement canvas,{int width, int height, StageOptions options}) : super(canvas,width: width, height: height, options: options);

  initStage();

  Future<BitmapData> loadResourceData(IDisplayResource displayResource) async {
    if(!_resourceManager.containsBitmapData(displayResource.getResourceName())){
      _resourceManager.addBitmapData(displayResource.getResourceName(),displayResource.getImgPath());
    }
    await _resourceManager.load();
    return _resourceManager.getBitmapData(displayResource.getResourceName());
  }
}