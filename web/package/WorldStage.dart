import 'dart:html';

import 'package:stagexl/stagexl.dart';

import 'AbstractDartStage.dart';
import 'GameManager.dart';
import 'data/Cell.dart';
import 'data/Player.dart';

/**
 * The world stage is the main stage displaying the game
 */
class WorldStage extends AbstractDartStage{

  /**
   * The graphics for the player
   */
  Sprite playerSprite;

  /**
   * Class constructor
   */
  WorldStage(CanvasElement canvas,{int width, int height, StageOptions options}) : super(canvas,width: width, height: height, options: options);

  /**
   * Renders the map and the player
   */
  @override
  initStage() {
    _renderMap();
    _renderPlayer();
  }

  /**
   * Renders the map
   */
  _renderMap() {
    List<List<Cell>> world = worldMap.getWorld();
    for(var y = 0; y < world.length; y++){
      var worldLine = world[y];
      for(var x = 0; x < worldLine.length; x++){
        _renderCell(worldLine[x]);
      }
    }
  }

  /**
   * Renders a specific cell
   */
  _renderCell(Cell cell) {
    loadResourceData(cell).then((bitmapData){
      var cellSprite = Sprite();
      cellSprite.addChild(Bitmap(bitmapData));

      cellSprite.x = cell.x * GameManager.CELL_SIZE_X;
      cellSprite.y = cell.y * GameManager.CELL_SIZE_Y;

      addChild(cellSprite);
    });
  }

  /**
   * Renders the player
   */
  _renderPlayer() {
    Player player = worldMap.player;
    loadResourceData(player).then((bitmapData){
      playerSprite = Sprite();
      playerSprite.addChild(Bitmap(bitmapData));

      refreshPlayer();

      addChild(playerSprite);
    });
  }

  /**
   * Refreshes the player position
   */
  refreshPlayer(){
    Player player = worldMap.player;
    playerSprite.x = player.x * GameManager.CELL_SIZE_X;
    playerSprite.y = player.y * GameManager.CELL_SIZE_Y;
  }

 }