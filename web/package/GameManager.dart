
import 'dart:html';

import 'package:stagexl/stagexl.dart';

import 'data/CopperMine.dart';
import 'data/DiamondMine.dart';
import 'data/EmptyCell.dart';
import 'data/GoldMine.dart';
import 'data/Mountain.dart';
import 'data/SilverMine.dart';
import 'data/WorldMap.dart';
import 'InfoStage.dart';
import 'WorldStage.dart';
import 'interfaces/IFrequency.dart';
import 'CellFactory.dart';
import 'util/Direction.dart';

/**
 * The tool managing the game
 */
class GameManager{

  /**
   * The pixel size for the x axis of the cells
   */
  static final CELL_SIZE_X = 40;
  /**
   * The pixel size for the y axis of the cells
   */
  static final CELL_SIZE_Y = 40;

  /**
   * The set of cells that will be used to generate the map
   */
  Set<IFrequency> CELL_TO_BE_GENERATED = Set.from([
    DiamondMine.empty(),
    CopperMine.empty(),
    GoldMine.empty(),
    SilverMine.empty(),
    Mountain.empty(),
    EmptyCell.empty(),
  ]);

  /**
   * The key code to mine (M)
   */
  static const KEY_CODE_MINE = 77;

  /**
   * The key code to empty the inventory (V)
   */
  static const KEY_CODE_DROP = 86;

  /**
   * The key codes for the different directions
   */
  static const KEY_CODE_DIRECTION = {
      37:Direction.WEST,
      38:Direction.NORTH,
      39:Direction.EAST,
      40:Direction.SOUTH
  };

  /**
   * The stage that contains the world
   */
  WorldStage worldStage;

  /**
   * The stage containing the player informations
   */
  InfoStage infoStage;

  /**
   * The loop used to render the graphics
   */
  RenderLoop renderLoop;

  /**
   * The game resource manager
   */
  ResourceManager resourceManager;

  /**
   * The world map
   */
  WorldMap worldMap;

  /**
   * Class constructor
   */
  GameManager(this.worldStage,this.infoStage) {
    resourceManager = ResourceManager();
    _initRenderLoop();
    worldMap = WorldMap((sizeX,sizeY) => CellFactory.generateListComponent(CELL_TO_BE_GENERATED, sizeX, sizeY) );
    worldStage.worldMap = worldMap;
    infoStage.worldMap = worldMap;
    worldStage.initStage();
    infoStage.initStage();
    _initKeyDownListener();
  }

  /**
   * Initializes the game render loop with the different stages
   */
  _initRenderLoop(){
    renderLoop = RenderLoop();
    renderLoop.addStage(worldStage);
    renderLoop.addStage(infoStage);
  }

  /**
   * Method listening to the keys pressed by the player and calling the associated action
   */
  _initKeyDownListener(){
      window.onKeyDown.listen((event) {
        try {
          if (KEY_CODE_DIRECTION.containsKey(event.keyCode)) {
            Direction direction = KEY_CODE_DIRECTION[event.keyCode];
            worldMap.doActionPlayerMove(direction);
            worldStage.refreshPlayer();
          } else if (KEY_CODE_MINE == event.keyCode) {
            worldMap.doActionPlayerMine();
            infoStage.refreshPlayerInventory();
          } else if (KEY_CODE_DROP == event.keyCode) {
            worldMap.doActionPlayerDischarge();
            infoStage.refreshPlayerInventory();
            infoStage.refreshBaseInventory();
          }
        }catch (e){
          infoStage.displayMessage(e);
        }
      });
  }

}

