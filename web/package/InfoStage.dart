import 'dart:html';

import 'package:stagexl/stagexl.dart';

import 'AbstractDartStage.dart';

/**
 * The info stages displays the player informations
 */
class InfoStage extends AbstractDartStage{

  TextField inventoryRobot;
  TextField inventoryRobotField;
  TextField inventoryRobotIsFull;

  TextField inventoryBase;
  TextField inventoryBaseField;

  TextField infoMessage;

  InfoStage(CanvasElement canvas,{int width, int height, StageOptions options}) : super(canvas,width: width, height: height, options: options);

  @override
  initStage() {
    _renderInfo();
    _renderPlayerInventory();
    _renderBaseInventory();
    _renderMessage();
  }

  _renderInfo(){
    inventoryRobot = new TextField("Inventory :");
    inventoryRobot.defaultTextFormat = new TextFormat('Russo One', 25, Color.Black);
    inventoryRobot.autoSize = TextFieldAutoSize.CENTER;
    inventoryRobot.x = 10;
    inventoryRobot.y = (stageHeight/2)-(inventoryRobot.height/2);
    inventoryRobot.wordWrap = true;
    addChild(inventoryRobot);

    inventoryBase = new TextField("Base :");
    inventoryBase.defaultTextFormat = new TextFormat('Russo One', 25, Color.Black);
    inventoryBase.autoSize = TextFieldAutoSize.CENTER;
    inventoryBase.x = 350;
    inventoryBase.y = (stageHeight/2)-(inventoryBase.height/2);
    inventoryBase.wordWrap = true;
    addChild(inventoryBase);

    inventoryRobotIsFull = new TextField("    Robot \n inventory \n    is full");
    inventoryRobotIsFull.defaultTextFormat = new TextFormat('Russo One', 20, Color.DarkRed);
    inventoryRobotIsFull.autoSize = TextFieldAutoSize.CENTER;
    inventoryRobotIsFull.x = 650;
    inventoryRobotIsFull.y = (stageHeight/2)-(inventoryRobotIsFull.height/2);
    inventoryRobotIsFull.wordWrap = true;
    inventoryRobotIsFull.visible = false;
    inventoryRobotIsFull.border =true;
    inventoryRobotIsFull.borderColor = Color.DarkRed;
    addChild(inventoryRobotIsFull);
  }

  _renderPlayerInventory(){
    inventoryRobotField =  new TextField();
    inventoryRobotField.defaultTextFormat = new TextFormat('Russo One', 20, Color.Black);
    inventoryRobotField.x = inventoryRobot.x + inventoryRobot.width+ 10;
    inventoryRobotField.y = 36;
    inventoryRobotField.width = 200;
    inventoryRobotField.height = stageHeight-36;
    inventoryRobotField.wordWrap = true;
    refreshPlayerInventory();
    addChild(inventoryRobotField);
  }

  refreshPlayerInventory(){
    var inventory = worldMap.player.inventory;
    var displayInventory = StringBuffer("");
    inventory.forEach((o,nb)  =>  displayInventory.writeln(" - ${o.value}: ${nb}"));
    inventoryRobotField.text = displayInventory.toString();
    inventoryRobotIsFull.visible= worldMap.player.isFull();
  }

  _renderBaseInventory(){
    inventoryBaseField =  new TextField();
    inventoryBaseField.defaultTextFormat = new TextFormat('Russo One', 20, Color.Black);
    inventoryBaseField.x = inventoryBase.x + inventoryBase.width+ 10;
    inventoryBaseField.y = 36;
    inventoryBaseField.width = 200;
    inventoryBaseField.height = stageHeight-36;
    inventoryBaseField.wordWrap = true;
    refreshBaseInventory();
    addChild(inventoryBaseField);
  }

  refreshBaseInventory(){
    var inventory = worldMap.base.baseInventory;
    var displayInventory = StringBuffer("");
    inventory.forEach((o,nb)  =>  displayInventory.writeln(" - ${o.value}: ${nb}"));
    inventoryBaseField.text = displayInventory.toString();
  }

  _renderMessage(){
    infoMessage = TextField();
    infoMessage.defaultTextFormat = new TextFormat('Russo One', 15, Color.DarkRed);
    infoMessage.autoSize = TextFieldAutoSize.CENTER;
    infoMessage.x = 350;
    infoMessage.y = 5;
    infoMessage.visible = false;
    addChild(infoMessage);
  }

  displayMessage(e){
    if(!infoMessage.visible){
      infoMessage.visible = true;
      infoMessage.text = e.toString();
      var tween = stage.juggler.addTween(infoMessage, 0.5, Transition.easeInOutCubic);
      tween.delay = 1;
      tween.onComplete = () => infoMessage.visible = false;
    }
  }

}