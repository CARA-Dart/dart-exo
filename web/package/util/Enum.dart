/**
 * A generic class used to associate an enum to a value
 */
abstract class Enum<T> {

  /**
   * The enumeration value
   */
  final T _value;

  /**
   * Class constructor
   */
  const Enum(this._value);

  /**
   * Returns the value of the enum
   */
  T get value => _value;
}