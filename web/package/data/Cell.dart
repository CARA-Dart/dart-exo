import '../interfaces/IDisplayResource.dart';

/**
 * A game cell
 */
abstract class Cell implements IDisplayResource{
  /**
   * The x axis position of the cell
   */
  int x;
  /**
   * The y position of the cell
   */
  int y;

  /**
   * Class constructor
   */
  Cell(this.x,this.y);

}