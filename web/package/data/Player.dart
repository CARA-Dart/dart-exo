

import '../interfaces/IDisplayResource.dart';
import 'Base.dart';
import 'Mine.dart';
import '../exception/EmptyMineException.dart';

/**
 * The player is the game main character that has to travel through the map and mine ores
 */
class Player implements IDisplayResource{

  /**
   * The image path of the player
   */
  static const _IMG_PATH = "images/player.png";
  /**
   * The name of the resource
   */
  static const _NAME = "player";

  /**
   * The size of the player inventory
   */
  static const INVENTORY_SIZE = 10;

  /**
   * The inventory of the player, that can contain ores
   */
  var inventory = Map();

  /**
   * The path to the player image
   */
  String imgPath;

  /**
   * The x position of the player
   */
  int x;

  /**
   * The y position of the player
   */
  int y;

  /**
   * The amount of ore carried by the player
   */
  int carriedOre = 0;

  /**
   * Class constructor
   */
  Player(this.x,this.y);

  /**
   * The action to get ore from a mine
   */
  void doActionMine(Mine mine) {
  }

  /**
   * Check is the inventory of the player is full
   */
  bool isFull() => true;

  /**
   * The player discharges his inventory into his base inventory
   */
  void doActionDischargeOresToBase(Base base){
  }

  /**
   * Returns the name of the resource
   */
  @override
  String getResourceName() => _NAME;

  /**
   * Returns the image path of the player
   */
  @override
  String getImgPath() => _IMG_PATH;


}