import 'Cell.dart';
import '../interfaces/IFrequency.dart';
import '../util/Ore.dart';

/**
 * A mine is a place where the player can dig resources
 */
abstract class Mine<T extends Cell> extends Cell {

  /**
   * The quantity of ores in the mine
   */
  int resourceQuantity;

  /**
   * Tells if the mine is empty or not
   */
  bool isEmpty;

  /**
   * Class constructor
   */
  Mine(this.resourceQuantity, int x, int y): super(x,y);

  /**
   * Mines the resources of the mine and returns the number of ore mined.
  */
  int mineOre(int canBeMined) => null;

  /**
   * Returns the number of ore that can be mined
   */
  int getMinedCount();

  /**
   * Returns the ore type
   */
  Ore getOreType();
}