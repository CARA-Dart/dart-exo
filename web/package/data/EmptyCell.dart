import 'Cell.dart';
import '../interfaces/IFrequency.dart';

/**
 * An empty cell is an area without anything on it
 */
class EmptyCell extends Cell{

  /**
   * The path of the cell image
   */
  static const _IMG_PATH = "images/empty.png";

  /**
   * The name of the resource
   */
  static const _NAME = "empty";

  /**
   * Class constructor
   */
  EmptyCell(int x, int y):super(x,y);

  /**
   * Class constructor without specific coordinates
   */
  EmptyCell.empty():super(0,0);

  /**
   * Returns the image path of the cell
   */
  @override
  String getImgPath() => _IMG_PATH;

  /**
   * Returns the name of the resource
   */
  @override
  String getResourceName() => _NAME;
}

