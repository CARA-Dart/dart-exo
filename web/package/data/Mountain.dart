import 'Cell.dart';
import '../interfaces/IFrequency.dart';

/**
 * A mountain is a place that cannot be crossed by the player
 */
class Mountain extends Cell{

  /**
   * The image path of the mountain
   */
  static const _IMG_PATH = "images/mountain.png";
  /**
   * The name of the resource
   */
  static const _NAME ="mountain";
  /**
   * The frequency for the mountain to appear
   */
  static const _FREQUENCY = 18;

  /**
   * Class constructor
   */
  Mountain(int x, int y) : super(x, y);
  /**
   * Class constructor for a mountain without specific coordinates
   */
  Mountain.empty() : super(0, 0);

  /**
   * Returns the image path of the mountain
   */
  @override
  String getImgPath() => _IMG_PATH;

  /**
   * Returns the name of the resource
   */
  @override
  String getResourceName() => _NAME;

}