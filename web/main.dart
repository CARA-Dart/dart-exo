import 'dart:html' as html;
import 'package:stagexl/stagexl.dart';

import 'package/GameManager.dart';
import 'package/InfoStage.dart';
import 'package/WorldStage.dart';

void main() async {
  StageOptions optionsWorld =  new StageOptions()
    ..backgroundColor = Color.Black
    ..renderEngine = RenderEngine.WebGL;

  var canvasWorld = html.querySelector('#stageWorld');
  var stageWorld = WorldStage(canvasWorld,width: 800, height: 800, options: optionsWorld);

  StageOptions optionsInfo =  new StageOptions()
    ..backgroundColor = Color.White
    ..renderEngine = RenderEngine.WebGL;

  var canvasInfo = html.querySelector('#stageInfo');
  var stageInfo = new InfoStage(canvasInfo, width: 800, height: 200, options: optionsInfo);

  GameManager(stageWorld,stageInfo);
}
