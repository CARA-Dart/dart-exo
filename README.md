# DartMiner

## Présentation

DartMiner est un jeu où vous contrôlez un robot mineur, qui comme son nom l'indique, doit se déplacer sur la planète Mars afin de miner différents minerais et les ramener à sa base afin de les stocker.
Parmi les minerais, on retrouve du :

- Copper
- Silver
- Gold
- Diamond

Le joueur peut se déplacer dans les 4 directions (haut, bas, gauche, droite) et possède un inventaire.
Il a également une base, qui possède également un inventaire et dans laquelle il peut déposer les minerais qu'il porte.
Toute la structure du projet est fournie, votre objectif étant d'implémenter certaines parties du code afin de le faire fonctionner.
Nous ne vous demandons de coder que de la logique métier, la partie rendu dans le navigateur est déjà gérée.

### Controles

**⇧** : Déplace le robot d'une case vers le Nord<br>
**⇩** : Déplace le robot d'une case vers le Sud<br>
**⇦** : Déplace le robot d'une case vers l'Ouest<br>
**⇨** : Déplace le robot d'une case vers l'Est<br>
**M** : Le robot mine le minerai qui se trouve sur sa case<br>
**V** : Le robot vide son inventaire de minerais dans sa base<br>

## Installation

1. Cloner le projet dans votre workspace : ```https://gitlab.com/CARA-Dart/dart-exo.git```
2. Télécharger la version stable 2.0.0 du SDK pour linux 64 bits sur https://www.dartlang.org/tools/sdk/archive
(Pour ne pas encombrer votre espace de travail sur les machines de l'université, dézippez l'archive dans votre dossier local)
     ```
     mv sdk-dart.zip /local/nomDeFamille
     unzip /local/nomDeFamille/sdk-dart.zip
     ```
3. Ouvrir un terminal à la racine de votre projet, celui-ci va vous servir durant l'ensemble du TP.
4. Dans le terminal, ajouter la variable d'environement de l'emplacement de votre SDK
    ```
    export PATH="$PATH":"/local/nomDeFamille/dart-sdk/bin"
    ```
5. Importer les dépendances du projet à l'aide du gestionnaire PUB
    ```
    pub get
    ```
6. Importer l'utilitaire webdev qui va vous permettre de lancer un server web
   ```
   pub global activate webdev
   ```
7. Ajouter la variable d'environnement de webdev
   ```
   export PATH="$PATH":"$HOME/.pub-cache/bin"
   ```

## IDE

Vous pouvez utiliser n'importe quel editeur de texte, mais nous vous recommandons d'utiliser :

* **WebStorm**
    1. Dans *settings > Languages & Frameworks > Dart* , renseigner le champ Dart SDK path.

* **Visual Studio Code**
    1. Installer l'extension Dart
    2. Indiquer l'emplacement du SDK, pour cela ouvrer le menu de command avec F1 et rechercher "Dart: change SDK"
    3. Renseigner le chemin d'accès du SDK de Dart.

## Commande à connaitre

* Pour lancer le projet:
   ```
   webdev serve
   ```
* Pour lancer un fichier de Test unitaire:
   ```
   pub run test test/fichierTest.dart
   ```
   
### N'hésitez pas à vous référer à la [documentation](https://www.dartlang.org/guides) et à nous solliciter :-)
#### Vous trouverez également le lien de la présentation Google Slide [ici](https://docs.google.com/presentation/d/1d_IaVt-UOnvUWhtUTIcVAiN8j-PbqZlVXPQJ46hNOvA/edit?usp=sharing)

## Exercices
### Exo 1

Créer la classe des mines pour les minerais suivant: Copper,Silver,Gold et Diamond.

Ces classes doivent hériter de la classe abstraite `Mine` ainsi que de l'ensemble de ses méthode abstraites.

Elles doivent également hériter des méthodes de l'interface `IDisplayResource` qui est implémentée dans la class `Cell` :
* Pour la méthode `String getImgPath()` renvoyer l'emplacement de l'image de la mine (par exemple "images/copper.png")
* Pour la méthode `String getResourceName()` renvoyer un nom unique pour la classe.

Ajouter dans ces classes un contructeur par défaut qui prend en paramètre un les coordonnées de la mine.

Ajouter dans ces classes un constucteur nommé "empty", qui a pour objectif de mettre les coordonnées de la mine x et y à 0.

### Exo 2

Dans la classe abstraite `Mine`, vérifier dans son constucteur qu'elle n'est pas vide.

Compléter la méthode `mineOre(int canBeMined)` qui prend en paramétre le nombre de minerais que le joueur veut miner et doit retourner le nombre de ressource qu'il peut réellement miner.
Il faut donc prendre en compte le nombre de minerais restant ainsi que le nombre de minerais qu'on peut miner en une action.
Ne pas oublier de décrémenter le nombre de ressources restantes dans la mine.

Pour vérifier qu'il n'y pas d'erreurs, lancer la classe de test unitaire `MineTest` :
```
pub run test test/MineTest.dart
```

### Exo 3

Créer la classe `Base` qui hérite de la `Cell`

Ajouter l'attribut `baseInventory` qui est une Map qui va contenir un type de minerai et le nombre de ressources.

Ajouter également la méthode `void add(Ore type, int nbr)`, qui prend en paramètre un type de minerai ainsi qu'une quantité.
Cette méthode à pour effet d'ajouter le contenu dans l'attribut inventory.

Pour vérifier qu'il n'y pas d'erreurs, lancer la classe de test unitaire `BaseTest` :
```
pub run test test/BaseTest.dart
```


### Exo 4

Dans la classe `Player`, compléter les méthodes :
* `void doActionMine(Mine mine)`, cette méthode réalise l'action de minage, elle fait appel à la méthode de minage d'une mine, en passant en paramètre le nombre de minerai que le joueur peut encore miner et ajoute les minerais dans son inventaire.
Créér également l'exception `EmptyMineException`, et la déclencher quand le joueur essaie de miner une mine vide.

* `void doActionDischargeOresToBase(Base base)`, cette méthode réalise l'action de vidage de l'inventaire du joueur dans la base, elle prend en paramètre une base dans laquelle le joueur va venir décharger son inventaire.

* `bool isFull()`, cette méthode vérifie si son inventaire est supérieur ou égal à sa taille max.

Pour vérifier qu'il n'y pas d'erreurs, lancer la classe de test unitaire `PlayerTest` :
```
pub run test test/PlayerTest.dart
```

### Exo 5

Dans la classe `WorldMap`,

Compléter la méthode privée `void_initWorld(List<Cell> worldCells)`, qui prend en paramètre une liste de `Cell` et qui ajoute l'ensemble dans la map du monde `_world` en leur attribuant des coordonnées.

Compléter la méthode `void doActionPlayerMove(Direction direction)`, qui prend en paramètre un point cardinal et modifie les coordonnées du joueur en fonction.
Attention si le joueur se trouve en bordure ou que la direction est invalide, créer et renvoyer une exception `InvalidDirectionException`.

Compléter la méthode `void doActionPlayerMine()`, cette méthode vérifie que l'utilisateur se trouve bien sur une mine et appelle la méthode `doActionMine` du joueur.
Attention si le joueur ne se trouve pas sur une mine, créer et renvoyer une exception `InvalidActionException`.

Compléter la méthode `void doActionPlayerDischarge()`, cette méthode vérifie que l'utilisateur se trouve bien sur sa base et appelle la méthode `doActionDischargeOresToBase` du joueur.
Attention si le joueur ne se trouve pas sur une base, créer et renvoyer une exception `InvalidActionException`.

Pour vérifier qu'il n'y pas d'erreurs, lancer la classe de test unitaire `WorldMapTest.dart` :
```
pub run test test/WorldMapTest.dart
```


### Exo 6

Implémenter l'interface IFrequency dans les classes ( Attention au type générique ):
```
DiamondMine
CopperMine
GoldMine
SilverMine
Mountain
EmptyCell
```

Dans la méthode `int getFrequency()` : renvoyer un nombre qui va correspondre à la fréquence d'apparition des différentes Cell.
Dans la méthode `T createNewInstance()`: renvoyer une nouvelle instance de la classe à l'aide du constructeur nommé empty.


<br>**L'exercice est terminé, vous pouvez maintenant joueur à DartMiner**
```
webdev serve
```



### Exo 7 ( Bonus )

Ajouter la possibilité au joueur d'utiliser plusieurs robots.
Le joueur pourra changer facilement de robot à l'aide du clique gauche de la souris.

Dans la classe `WorldMap` modifier le conportement de la classe pour qu'elle puisse gérer un tableau de robots, l'ensemble des robots commenceront sur la base.
Attention les robots ne peuvent pas être sur la même cellule en même temps, à l'exception de la base.

Ajouter dans la classe `GameManager` une méthode pour récupérer les événements lors du clique gauche de la souris, vous pouvez vous inspirer de cette exemple:
```
window.onMouseDown.listen((event){
    if(event.button == 0){
        //do something
    }
});
```

Dans la classe `InfoStage`, la méthode `refreshPlayerInventory()` permet de rafraichir le panel d'informations sur l'inventaire du robot, il faudra donc adapter cette méthode pour n'afficher unique les informations sur le robot en cours d'utilisation.

Dans la classe `WorldStage`, nous avons l'attribut `playerSprite` qui correspond au sprite du robot, il faudra donc le modifier pour garder en mémoire les sprites de chacun des robots.
La méthode `refreshPlayer()` permet de rafraichir et déplacer le robot à  l'écran, modifier cette méthode pour ne rafraichir uniquement le robot en cours d'utilisation.