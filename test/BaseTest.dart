import "package:test/test.dart";

import '../web/package/data/Base.dart';
import '../web/package/util/Ore.dart';

main(){
  group("Base", () {
    test("add() with values", () {
      var base = Base(0,0);
      base.add(Ore.COPPER, 10);
      expect(base.baseInventory, equals({Ore.COPPER: 10}));
    });
    test("add() with null values", () {
      var base = Base(0,0);
      base.add(null, null);
      expect(base.baseInventory, equals({null:null}));
    });
    test("add() add with existing ore", () {
      var base = Base(0,0);
      base.add(Ore.COPPER, 10);
      base.add(Ore.COPPER, 10);
      expect(base.baseInventory, equals({Ore.COPPER: 20}));
    });
  });
}