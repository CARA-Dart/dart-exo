import 'package:quiver/iterables.dart';

import '../web/package/data/Player.dart';
import '../web/package/data/Base.dart';
import '../web/package/exception/EmptyMineException.dart';
import '../web/package/util/Ore.dart';
import 'mock/MineMock.dart';
import "package:test/test.dart";

main(){
  group("Player", () {
    test("doActionMine() with sufficient space and mine is not empty", () {
      var player = Player(0,0);
      var mine = MineMock(5,0,0);
      mine.minecount=3;
      player.doActionMine(mine);
      expect(player.carriedOre, equals(3));
    });
    test("doActionMine() but mine is empty", () {
      var player = Player(0,0);
      var mine = MineMock(0,0,0);
      mine.minecount=3;

      try{
        player.doActionMine(mine);
        expect(false, equals(true));
      } on EmptyMineException {
        expect(true, equals(true));
      }

    });
    test("doActionMine() with sufficient space and mine is not empty, the ore is added to the inventory map", () {
      var player = Player(0,0);
      var mine = MineMock(5,0,0);
      mine.TYPE=Ore.COPPER;
      mine.minecount=3;
      player.doActionMine(mine);
      expect(player.inventory, contains(Ore.COPPER));
    });
    test("doActionMine() with sufficient space and mine is not empty, the player already has this ore type", () {
      var player = Player(0,0);
      player.inventory[Ore.COPPER]=3;
      var mine = MineMock(5,0,0);
      mine.TYPE=Ore.COPPER;
      mine.minecount=3;
      player.doActionMine(mine);
      expect(player.inventory[Ore.COPPER], equals(6));

    });
    test("doActionDischargeOresToBase(), the base inventory is empty", () {
      var player = Player(0,0);
      player.inventory[Ore.COPPER]=3;
      player.inventory[Ore.GOLD]=5;
      var base = Base(0,0);
      player.doActionDischargeOresToBase(base);
      expect(base.baseInventory[Ore.GOLD], equals(5));
      expect(base.baseInventory[Ore.COPPER], equals(3));

    });

    test("doActionDischargeOresToBase(), the player inventory must then be null", () {
      var player = Player(0,0);
      player.inventory[Ore.COPPER]=3;
      player.inventory[Ore.GOLD]=5;
      var base = Base(0,0);
      player.doActionDischargeOresToBase(base);
      expect(player.inventory[Ore.COPPER], equals(null));
      expect(player.inventory[Ore.GOLD], equals(null));

    });

    test("doActionDischargeOresToBase(), the base inventory has not this ore type", () {
      var player = Player(0,0);
      player.inventory[Ore.COPPER]=3;
      player.inventory[Ore.GOLD]=5;
      var base = Base(0,0);
      player.doActionDischargeOresToBase(base);
      expect(base.baseInventory, contains(Ore.COPPER));
      expect(base.baseInventory, contains(Ore.GOLD));


    });
  });
}