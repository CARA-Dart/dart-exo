import '../../web/package/data/Cell.dart';
import '../../web/package/data/Mine.dart';
import '../../web/package/util/Ore.dart';

class MineMock extends Mine<Mine>{

  var frequency;
  var imgpath;
  var minecount;
  var TYPE;
  var ressourceName;

  MineMock(int resourceQuantity, int x, int y) : super(resourceQuantity, x, y);
  MineMock.empty():super(0,0,0);

  @override
  int getFrequency() {
    return frequency;
  }

  @override
  String getImgPath() {
    return imgpath;
  }

  @override
  int getMinedCount() {
    return minecount;
  }

  @override
  Ore getOreType() {
    return TYPE;
  }

  @override
  String getResourceName() {
    return ressourceName;
  }

  @override
  Mine<Cell> createNewInstance() {
    return new MineMock(resourceQuantity, x, y);
  }

}